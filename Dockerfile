FROM golang:1.13 AS builder
RUN go get github.com/SuperQ/smokeping_prober
RUN cd /go/src/github.com/SuperQ/smokeping_prober && make build

FROM scratch
COPY --from=builder /go/src/github.com/SuperQ/smokeping_prober/smokeping_prober /smokeping_prober
EXPOSE 9374
ENTRYPOINT ["/smokeping_prober"]
